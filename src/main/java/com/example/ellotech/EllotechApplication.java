package com.example.ellotech;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EllotechApplication {

	public static void main(String[] args) {
		SpringApplication.run(EllotechApplication.class, args);
	}

}
