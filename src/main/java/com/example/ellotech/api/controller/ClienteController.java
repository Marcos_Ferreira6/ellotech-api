package com.example.ellotech.api.controller;

import com.example.ellotech.domain.cliente.model.Cliente;
import com.example.ellotech.domain.cliente.service.ClienteService;
import com.example.ellotech.domain.cliente.view.ClienteView;
import com.fasterxml.jackson.annotation.JsonView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/clientes")
@CrossOrigin(exposedHeaders="Access-Control-Allow-Origin")
public class ClienteController {

    @Autowired
    ClienteService clienteService;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    @JsonView(ClienteView.ClienteViewResumido.class)
    public List<Cliente> findAllClientes(Pageable pageable) {
        return clienteService.findAll(pageable);
    }

    @GetMapping("/search")
    public Page<Cliente> searchClientes(@RequestParam("searchTerm") String searchTerm, Pageable pageable) {
        return clienteService.search(searchTerm, pageable);
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Cliente> findClienteById(@PathVariable("id") int id) {
        Optional<Cliente> cliente = clienteService.findById(id);
        return cliente.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }

    @GetMapping("/exists")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Boolean> findClienteByCpf(@RequestParam("cpf") String cpf) {
        return ResponseEntity.ok().body(clienteService.findByCpf(cpf));
    }

    @GetMapping("/count")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Long> countClientes() {
        return ResponseEntity.ok().body(clienteService.count());
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Cliente postCliente(@RequestBody @Valid Cliente cliente) {
        return clienteService.save(cliente);
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Cliente> putCliente(@RequestBody @Valid Cliente cliente, @PathVariable("id") int id) {
        cliente = clienteService.update(cliente, id);
        return ResponseEntity.ok(cliente);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity<Void> deleteCliente(@PathVariable("id") int id) {
        clienteService.delete(id);
        return ResponseEntity.noContent().build();
    }
}
