package com.example.ellotech.domain.cliente.model;

import com.example.ellotech.domain.cliente.view.ClienteView;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonView;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.br.CPF;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.Date;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Table(name = "cliente")
public class Cliente {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonView(ClienteView.ClienteViewResumido.class)
    private Integer id;

    @NotBlank
    @Size(max = 75)
    @JsonView(ClienteView.ClienteViewResumido.class)
    private String nome;

    @NotBlank
    @Size(max = 255)
    @Email
    @JsonView(ClienteView.ClienteViewResumido.class)
    private String email;

    @NotBlank
    @Size(max=20)
    @JsonView(ClienteView.ClienteViewResumido.class)
    private String telefone;

    @NotBlank
    @Size(max = 1)
    @Pattern(regexp = "|\\bM\\b|\\bF\\b", message = "O Sexo deve ser 'M' (Masculino), 'F' (Feminino)")
    @JsonView(ClienteView.ClienteViewResumido.class)
    private String sexo;

    @NotNull
    @Temporal(TemporalType.DATE)
    @JsonView(ClienteView.ClienteViewResumido.class)
    @JsonProperty("data_nascimento")
    private Date dataNascimento;

    @NotBlank
    @CPF
    @Size(max = 14)
    @JsonView(ClienteView.ClienteViewResumido.class)
    private String cpf;
}
