package com.example.ellotech.domain.cliente.repository;

import com.example.ellotech.domain.cliente.model.Cliente;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestParam;

@Repository
public interface ClienteRepository extends JpaRepository<Cliente, Integer> {

    @Query("FROM Cliente c WHERE LOWER(c.cpf) LIKE %:searchTerm%")
    Page<Cliente> search(@RequestParam("searchTerm") String searchTerm, Pageable pageable);
    Page<Cliente> findAll(Pageable pageable);

    boolean existsByCpf(String cpf);
}
