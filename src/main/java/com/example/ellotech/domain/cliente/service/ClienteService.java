package com.example.ellotech.domain.cliente.service;

import com.example.ellotech.domain.cliente.model.Cliente;
import com.example.ellotech.domain.cliente.repository.ClienteRepository;
import com.example.ellotech.exception.NegocioException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    ClienteRepository clienteRepository;

    public List<Cliente> findAll(Pageable pageable) {
        Page<Cliente> clientes = clienteRepository.findAll(PageRequest.of(
                pageable.getPageNumber(), pageable.getPageSize()));
        return clientes.getContent();
    }

    public Page<Cliente> search(@RequestParam("searchTerm") String searchTerm, Pageable pageable) {
        PageRequest pageRequest = PageRequest.of(
                pageable.getPageNumber(), pageable.getPageSize(), Sort.Direction.ASC, "cpf");
        return clienteRepository.search(searchTerm.toLowerCase(), pageRequest);
    }

    public Optional<Cliente> findById(Integer id) {
        Optional<Cliente> cliente = clienteRepository.findById(id);
        if (cliente.isPresent()) {
            return cliente;
        }

        throw new NegocioException("Nenhum Cliente encontrado com o ID informado");
    }

    public Cliente save(Cliente cliente) {
        boolean clienteExistente = findByCpf(cliente.getCpf());
        if (clienteExistente) {
            throw new NegocioException("O CPF informado já existe");
        }

        return clienteRepository.save(cliente);
    }

    public Cliente update(Cliente cliente, int id) {
        if (!(clienteRepository.findById(id).isPresent())) {
            throw new NegocioException("Nenhum Cliente encontrado com o ID informado");
        }

        cliente.setId(id);
        return clienteRepository.save(cliente);
    }

    public boolean delete(Integer id) {
        Optional<Cliente> cliente = clienteRepository.findById(id);
        if(cliente.isPresent()) {
            clienteRepository.delete(cliente.get());
            return true;
        }

        throw new NegocioException("Nenhum Cliente encontrado com o ID informado");
    }

    public long count() { return clienteRepository.count(); }

    public boolean findByCpf(String cpf) {
        return clienteRepository.existsByCpf(cpf);
    }
}
