package com.example.ellotech.domain.cliente.view;

public class ClienteView {
    public interface ClienteViewResumido {};
    public interface ClienteViewCompleto extends ClienteViewResumido {};
}
