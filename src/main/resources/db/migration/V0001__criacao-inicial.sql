create table cliente (
	id bigint auto_increment not null,
    nome varchar(75) not null,
    email varchar(255) not null,
	telefone varchar(20) not null,
    sexo char(1) not null,
	cpf char(14) not null,
    data_nascimento date not null,

	primary key(id)
);

insert into `cliente` values
    (1, 'Jonas', 'jonas@kanhwald', '3653-6698', 'M', '456.656.548.65', '1997-06-06'),
    (2, 'Martha', 'martha@nielsen', '3652-7896', 'F', '456.656.548.65', '1997-03-15');